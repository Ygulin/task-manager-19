package ru.tsc.gulin.tm.api.repository;

import ru.tsc.gulin.tm.model.Project;

public interface IProjectRepository extends IRepository<Project> {

    Project create(String name);

    Project create(String name, String description);

}
