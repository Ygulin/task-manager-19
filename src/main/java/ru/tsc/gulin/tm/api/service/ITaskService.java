package ru.tsc.gulin.tm.api.service;

import ru.tsc.gulin.tm.enumerated.Status;
import ru.tsc.gulin.tm.model.Task;
import java.util.Date;
import java.util.List;

public interface ITaskService extends IService<Task> {

    List<Task> findAllByProjectId(String projectId);

    Task create(String name);

    Task create(String name, String description);

    Task create(String name, String description, Date dateBegin, Date dateEnd);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

}
